#Zentora Theme  

* Version: 1.0.6
* [Forums](http://forums.sentora.org/showthread.php?tid=160) for comments, discussion, more information and community support.


##Description

A light, clean, simple and trying to be professional looking Bootstrap based theme for [Sentora](http://sentora.org/).

##Downloading Zentora

###To use this theme in Sentora:
1. a)
    * Download the [master](https://github.com/auxio/Zentora/archive/master.zip) branch.
    *  Extract **Zentora-master.zip** and rename the directory from **Zentora-master** to **Zentora**.
    * Upload your **Zentora** directory into **/etc/sentora/panel/etc/styles** directory on your Sentora server.

  b)
  * Clone this repo into **/etc/sentora/panel/etc/styles**  
  `git clone git://github.com/fa7ad/Zentora.git /etc/sentora/panel/etc/styles/Zentora`
2. Go to your Sentora **Admin** => **Reseller** => **Theme Manager** choose **Zentora** and save.
3. If you like you may choose a theme variation.

##License agreement

Zentora is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0).

So it can be used for **personal and commercial** purposes, but **please do not remove** the **powered by _(Auxio logo)_**!

##Author

This theme is designed by @Ron-e and edited by @fa7ad for my uses.

##Credits

This theme uses:
* [Bootstrap v3.1.1](http://getbootstrap.com)
* [jQuery v1.11.1](http://jquery.org)
* [Sentora logo's](https://github.com/sentora/art)
* And lots if random things from the default Sentora theme by jasondavis.

##Getting support

If you have a question of found a bug you are free to comment in [this thread](http://forums.sentora.org/showthread.php?tid=160) on the [Sentora forums](http://forums.sentora.org/) or create an Issue here.

##Version updates
* 1.0.6a
  *  Edited for my use -- @fa7ad
* 1.0.6
  * added colored icon for the protected directories module.
  * added meta to headers (special thanks to TGates)

* 1.0.5
  * Removed JS error
  * Fixed input, select, and textarea styles
  * Some small things (special thanks to VPenkov).
* 1.0.4.1
  * Fix dark theme logo paths
* 1.0.4
  * Fixed renamed Zpanel.js
* 1.0.3
  * Standardised to new Sentora's structure policy. (special thanks to PS2Guy aka Nigel).
* 1.0.2
  * Fixed button spacing and z-index from search wrapper, version number changed to 1.0.2.
  * re-branded ZstyleX to Zentora, version number is still 10.0.2.
* 1.0.1
  * Fixed non-standard spaces bug.
* 1.0.0
  * Uploaded first version

##WARRANTY

THE THEME IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT WITHOUT ANY WARRANTY.
IT IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE THEME IS WITH YOU.
SHOULD THE THEME PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW THE AUTHOR WILL BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE THEME
(INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE THEME TO OPERATE WITH ANY OTHER PROGRAMS),
EVEN IF THE AUTHOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
